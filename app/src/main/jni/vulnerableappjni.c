#include <jni.h>
#include <string.h>

JNIEXPORT void JNICALL
Java_org_sid_vulnerableappjni_VulnerableAppJNI_memCorrupt(JNIEnv *env, jclass type, jstring s_) {
    const char *s = (*env)->GetStringUTFChars(env, s_, 0);
    char buf[128];
    strcpy(buf,s);
    (*env)->ReleaseStringUTFChars(env, s_, s);
}

JNIEXPORT void JNICALL
Java_org_sid_vulnerableappjni_VulnerableAppJNI_cippa(JNIEnv *env, jclass type) {

    // TODO

}

JNIEXPORT void JNICALL
Java_org_sid_vulnerableappjni_Generic_memCorrupt(JNIEnv *env, jclass type, jstring s_) {
    const char *s = (*env)->GetStringUTFChars(env, s_, 0);
    char buf[128];
    strcpy(buf,s);
    (*env)->ReleaseStringUTFChars(env, s_, s);
}