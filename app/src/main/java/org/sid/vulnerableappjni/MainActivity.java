package org.sid.vulnerableappjni;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(org.sid.vulnerableappjni.R.layout.activity_main);

    }

    public void startVulnApp(View v){
        Intent i = new Intent(this, VulnerableAppJNI.class);
        i.putExtra("input","user-controllable");
        startActivity(i);
    }
    public void startMediaApp(View v){
        Intent i = new Intent(this, MediaActivity.class);
        i.putExtra("input","user-controllable");
        startActivity(i);
    }
}
