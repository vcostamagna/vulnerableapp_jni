package org.sid.vulnerableappjni;

import android.util.Log;

/**
 * Created by vaioco on 06/07/16.
 */

public class TypeA extends Generic {
    @Override
    public void MOI(String text) {
        Log.d(Commons.TAG, "Called TypeA MOI");
        StringBuffer outputBuffer = new StringBuffer(bufsize);
        outputBuffer.append(text);
        //for (int i = 0; i < bufsize; i++){
        //    outputBuffer.append("A");
        //}
        //Log.d(Commons.TAG, "triggering with: " + outputBuffer.toString());
        VulnerableAppJNI.memCorrupt(outputBuffer.toString());
    }
}
