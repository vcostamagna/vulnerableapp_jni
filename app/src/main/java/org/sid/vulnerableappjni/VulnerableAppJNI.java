package org.sid.vulnerableappjni;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class VulnerableAppJNI extends Activity {
    public static int bufsize = 2048;
    static {
        System.loadLibrary("native");
    }

    public static native void memCorrupt(String s);
    //public static native void cippa();

    public static void triggerMemCorr1(String input){
        StringBuffer outputBuffer = new StringBuffer(bufsize);
        outputBuffer.append(input);
        //for (int i = 0; i < bufsize; i++){
        //    outputBuffer.append("A");
        //}
        //Log.d(Commons.TAG, "triggering with: " + outputBuffer.toString());
        //memCorrupt(outputBuffer.toString());
    }
    /*
    public static void triggerMemCorr(){
        StringBuffer outputBuffer = new StringBuffer(bufsize);
        for (int i = 0; i < bufsize; i++){
            outputBuffer.append("A");
        }
        //Log.d(Commons.TAG, "triggering with: " + outputBuffer.toString());
        memCorrupt(outputBuffer.toString());
    }
    */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String input = "";
        super.onCreate(savedInstanceState);
        setContentView(org.sid.vulnerableappjni.R.layout.activity_vulnerable_app_jni);
        Log.d(Commons.TAG,"Activity started");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            input = extras.getString("input");
        }
        //triggerMemCorr1(input);
        //triggerMemCorr();

        Generic a1 = new TypeA();
        Generic b1 = new TypeB();
        //a1.MOI(input);


    }
}
